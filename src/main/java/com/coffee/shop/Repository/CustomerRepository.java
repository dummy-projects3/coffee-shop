package com.coffee.shop.Repository;

import com.coffee.shop.entity.Customer;
import com.coffee.shop.entity.Drink;

import java.util.HashSet;
import java.util.Set;

public class CustomerRepository {
    private static CustomerRepository customerRepository = null;

    private CustomerRepository() {
    }

    public static CustomerRepository getInstance() {
        if (null == customerRepository) {
            customerRepository = new CustomerRepository();
        }
        return customerRepository;
    }

    public static Set<Customer> dbCustomers = new HashSet<>();

    public Customer getCustomerByEmail(String email) {
        return dbCustomers.stream().filter(customer -> customer.getName().equalsIgnoreCase(email)).findFirst().orElse(null);
    }

    public Set<Customer> getCustomers() {
        Set<Customer> customers = new HashSet<>();
        customers.addAll(dbCustomers);
        return customers;
    }

    public Boolean deleteCustomer(Customer customer) {
        return dbCustomers.remove(customer) ? true : false;
    }

    public Customer saveCustomer(Customer customer) {
        return addUpdateCustomerInfo(customer);
    }

    public Customer updateCustomer(Customer customer) {
        return addUpdateCustomerInfo(customer);
    }

    private Customer addUpdateCustomerInfo(Customer customer) {
        dbCustomers.add(customer);
        return customer;
    }
}
