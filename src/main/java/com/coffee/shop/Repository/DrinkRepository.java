package com.coffee.shop.Repository;

import com.coffee.shop.entity.Drink;

import java.util.HashSet;
import java.util.Set;

public class DrinkRepository {

    private static DrinkRepository drinkRepository = null;

    private DrinkRepository() {
    }

    public static DrinkRepository getInstance() {
        if (null == drinkRepository) {
            drinkRepository = new DrinkRepository();
        }
        return drinkRepository;
    }

    public static Set<Drink> drinks = new HashSet<>();

    public Drink getDrinkByName(String drinkName) {
        return drinks.stream().filter(drink -> drink.getName().equalsIgnoreCase(drinkName)).findFirst().get();
    }

    public Set<Drink> getDrinks() {
        Set<Drink> dbDrinks = new HashSet<>();
        dbDrinks.addAll(drinks);
        return dbDrinks;
    }

    public Boolean deleteDrink(Drink drink) {
        return drinks.remove(drink) ? true : false;
    }

    public Drink saveDrink(Drink drink) {
        return addDrink(drink);
    }

    public Drink updateDrink(Drink drink) {
        return updateDrink(drink);
    }

    private Drink addDrink(Drink drink) {
        drinks.add(drink);
        return drink;
    }

}
