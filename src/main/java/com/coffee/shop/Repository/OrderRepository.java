package com.coffee.shop.Repository;

import com.coffee.shop.entity.Order;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class OrderRepository {

    LinkedHashSet<Order> orders = new LinkedHashSet<>();
    private static OrderRepository orderRepository = null;

    private OrderRepository() {
    }

    public static OrderRepository getInstance() {
        if (null == orderRepository) {
            orderRepository = new OrderRepository();
        }
        return orderRepository;
    }

    public Order createOrder(Order order) {
        if (orders.contains(order)) {
            throw new RuntimeException("Order ID already exist, Cannot create order for shared orderId");
        }
        orders.add(order);
        return order;
    }

    public Order updateOrder(Order order) {
        if (!orders.contains(order) && null == order.getOrderId()) {
            throw new RuntimeException("Cannot update order as orderId is null/empty or does not exist");
        }
        orders.add(order);
        return order;
    }

    public Order findOrderByOrderId(Integer orderId) {
        return orders.stream().filter(order -> order.getOrderId().equals(orderId)).findFirst().orElse(null);
    }

    public List<Order> findOrderByCustomerEmail(String email) {
        return orders.stream().filter(order -> order.getCustomer().getEmail().equalsIgnoreCase(email)).collect(Collectors.toList());
    }

}
