package com.coffee.shop.service;

import com.coffee.shop.Repository.OrderRepository;
import com.coffee.shop.entity.Customer;
import com.coffee.shop.entity.Drink;
import com.coffee.shop.entity.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderService {
    private OrderRepository orderRepository = OrderRepository.getInstance();
    private CustomerService customerService = CustomerService.getInstance();
    private static OrderService orderService;

    private OrderService() {
    }

    public static OrderService getInstance() {
        if (null == orderService) {
            orderService = new OrderService();
        }
        return orderService;
    }

    public Order createOrder(Order userOrder) {
        Order dbOrder = getOrderByOrderId(userOrder.getOrderId());
        if (null == dbOrder) {
            Order addedOrder = orderRepository.createOrder(userOrder);
            System.out.println("Item added to order : ");
            addedOrder.getDrinks().stream().forEach(System.out::println);
            return addedOrder;
        } else {
            List<Drink> updatedDrinksOrder = new ArrayList<>();
            List<Drink> alreadyPresentDrinksWithOrder = dbOrder.getDrinks();
            updatedDrinksOrder.addAll(alreadyPresentDrinksWithOrder);
            updatedDrinksOrder.addAll(userOrder.getDrinks());
            dbOrder.setDrinks(updatedDrinksOrder);
            return updateOrder(dbOrder);
        }

    }

    public Order updateOrder(Order order) {
        Order updatedOrder = orderRepository.updateOrder(order);
        System.out.println("Item updated in order : ");
        updatedOrder.getDrinks().stream().forEach(System.out::println);
        return order;
    }

    public Order cancelOrder(Integer orderId) {
        Order dborder = orderRepository.findOrderByOrderId(orderId);
        if (null == dborder) {
            System.out.println("Cannot cancel order as order doesn't exists for order ID : " + orderId);
        }
        dborder.setOrderCancelled(true);
        return updateOrder(dborder);
    }


    public Order placeOrder(Integer orderId, Customer customer) {
        Order dborder = orderRepository.findOrderByOrderId(orderId);
        if (null == dborder) {
            System.out.println("Cannot place order as order doesn't exists for order ID : " + orderId);
        }

        dborder.setCustomer(customer);
        dborder.setOrderPlaced(true);
        Order order = updateOrder(dborder);
        customerService.addCustomer(customer);
        return order;
    }

    public Order getOrderByOrderId(Integer orderId) {
        Order order = orderRepository.findOrderByOrderId(orderId);
        return order;
    }

    public List<Order> getCustomerOrderHistory(String email) {
        List<Order> customerHistoryOrder = orderRepository.findOrderByCustomerEmail(email);
        return customerHistoryOrder;
    }

}
