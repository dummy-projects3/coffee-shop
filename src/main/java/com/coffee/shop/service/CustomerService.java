package com.coffee.shop.service;

import com.coffee.shop.Repository.CustomerRepository;
import com.coffee.shop.entity.Customer;
import org.apache.commons.lang.StringUtils;

import static java.lang.System.*;

public class CustomerService {
    private CustomerRepository customerRepository = CustomerRepository.getInstance();
    private static CustomerService customerService = null;

    private CustomerService() {
    }

    public static CustomerService getInstance() {
        if (null == customerService) {
            customerService = new CustomerService();
        }
        return customerService;
    }

    public Customer addCustomer(Customer customer) {
        if (!valiatedCustomer(customer)) {
            out.println("Customer name or email cannot be empty or null");
            return null;
        }
        Customer existingCustomer = customerRepository.getCustomerByEmail(customer.getEmail());
        if (null == existingCustomer) {
            return customerRepository.saveCustomer(customer);
        } else {
            return updateCustomer(customer);
        }
    }

    public Customer updateCustomer(Customer customer) {
        if (valiatedCustomer(customer)) {
            return customerRepository.updateCustomer(customer);
        } else {
            out.println("Customer name or email cannot be empty or null");
            return null;
        }
    }

    private Boolean valiatedCustomer(Customer customer) {
        return (StringUtils.isNotEmpty(customer.getName()) && StringUtils.isNotEmpty(customer.getEmail())) ? true : false;
    }
}
