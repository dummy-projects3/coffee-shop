package com.coffee.shop.service;

import com.coffee.shop.Repository.DrinkRepository;
import com.coffee.shop.entity.Drink;
import com.coffee.shop.model.DrinkMenu;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DrinkService {

    DrinkRepository drinkRepository = DrinkRepository.getInstance();
    private static DrinkService drinkService = null;

    private DrinkService() {
    }

    public static DrinkService getInstance() {
        if (null == drinkService) {
            drinkService = new DrinkService();
        }
        return drinkService;
    }

    public Boolean loadStartupMenuDrink() {
        drinkRepository.saveDrink(new Drink("Filter Coffee", 10.00, Arrays.asList("Roasted crushed coffee, milk, sugar, hot water")));
        drinkRepository.saveDrink(new Drink("Cold Coffee", 50.00, Arrays.asList("Nescafe", "Ice cold Milk", "Choclate Sirup", "Choclate flvoured sugar")));
        return true;
    }

    public List<DrinkMenu> getDrinksMenu() {
        List<DrinkMenu> drinksMenu = drinkRepository.getDrinks().stream().map(drink -> new DrinkMenu(drink.getName(), drink.getPrice())).collect(Collectors.toList());
        return drinksMenu;
    }

    public Drink getDrinkByName(String drinkName) {
        Drink drink = drinkRepository.getDrinkByName(drinkName);
        return drink;
    }

    public Drink addDrink(Drink drink) {
        Drink addedDrink = drinkRepository.saveDrink(drink);
        System.out.println("Drink Updated successfully : " + addedDrink.toString());
        return addedDrink;
    }

    public Drink updateDrink(Drink drink) {
        Drink updateDrink = drinkRepository.updateDrink(drink);
        System.out.println("Drink Updated successfully : " + updateDrink.toString());
        return updateDrink;
    }

    public void removeDrink(Drink drink) {
        drinkRepository.deleteDrink(drink);
        System.out.println("Drink removed as menu item from DB");
    }
}
