package com.coffee.shop.entity;

import java.util.List;
import java.util.Objects;

public class Order {
    private Integer orderId;
    private Customer customer;
    private List<Drink> drinks;
    private Boolean isOrderPlaced = false;
    private Boolean isOrderCancelled = false;

    public Order() {
    }

    public Order(Integer orderId, Customer customer, List<Drink> drinks) {
        this.orderId = orderId;
        this.customer = customer;
        this.drinks = drinks;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public Boolean getOrderPlaced() {
        return isOrderPlaced;
    }

    public void setOrderPlaced(Boolean orderPlaced) {
        isOrderPlaced = orderPlaced;
    }

    public Boolean getOrderCancelled() {
        return isOrderCancelled;
    }

    public void setOrderCancelled(Boolean orderCancelled) {
        isOrderCancelled = orderCancelled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId.equals(order.orderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", customer=" + customer +
                ", drinks=" + drinks +
                ", isOrderPlaced=" + isOrderPlaced +
                ", isOrderCancelled=" + isOrderCancelled +
                '}';
    }
}
