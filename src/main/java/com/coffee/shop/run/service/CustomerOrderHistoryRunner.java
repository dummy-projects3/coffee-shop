package com.coffee.shop.run.service;

import com.coffee.shop.entity.Order;
import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.OrderService;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Scanner;

public class CustomerOrderHistoryRunner implements CoffeeShopBaseRunner {
    private OrderService orderService = OrderService.getInstance();

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        List<Order> orders = getCustomerOrderHistory();
        if (CollectionUtils.isEmpty(orders)) {
            System.out.println("Customer is yet place order with coffee shop");
            System.out.println("Please proceed for creating order for customer");
        } else {
            System.out.println("Customer history order till date : ");
            orders.stream().forEach(System.out::println);
        }
    }

    private List<Order> getCustomerOrderHistory() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter customers email for order history");
        String email = scanner.nextLine();
        List<Order> orders = orderService.getCustomerOrderHistory(email);
        return orders;
    }
}
