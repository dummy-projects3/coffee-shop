package com.coffee.shop.run.service;

import com.coffee.shop.entity.Drink;
import com.coffee.shop.entity.Order;
import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.DrinkService;
import com.coffee.shop.service.OrderService;

import java.util.Arrays;
import java.util.Scanner;

public class AddItemOrderRunner implements CoffeeShopBaseRunner {

    private OrderService orderService = OrderService.getInstance();
    private DrinkService drinkService = DrinkService.getInstance();

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        Order order = getOrderDetails(coffeeShopContext);
        orderService.createOrder(order);
        System.out.println("Order initialized successfully");
    }

    private Order getOrderDetails(CoffeeShopContext coffeeShopContext) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter drinks as per listed menu item OR press ``SPACE BAR`` to go back main menu");
        String orderItem = scanner.nextLine();
        if (orderItem.equalsIgnoreCase(" ")) {
            return null;
        }
        Drink drink = getDrinkByName(orderItem);
        if (null == drink) {
            return null;
        }
        return getOrder(coffeeShopContext, drink);
    }

    private Order getOrder(CoffeeShopContext coffeeShopContext, Drink drink) {
        Order order = new Order();
        order.setOrderPlaced(false);
        order.setOrderId(coffeeShopContext.getOrderNumber());
        order.setDrinks(Arrays.asList(drink));
        return order;
    }

    private Drink getDrinkByName(String drinkName) {
        Drink drink = drinkService.getDrinkByName(drinkName);
        if (null == drink) {
            System.out.println("Entered drink doesn't exist. Please enter selected drink name.");
        }
        return drink;
    }
}
