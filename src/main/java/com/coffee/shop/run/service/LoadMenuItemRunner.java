package com.coffee.shop.run.service;

import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.DrinkService;

public class LoadMenuItemRunner implements CoffeeShopBaseRunner {

    DrinkService drinkService = DrinkService.getInstance();

    public LoadMenuItemRunner() {
        drinkService.loadStartupMenuDrink();
    }

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        drinkService.getDrinksMenu().stream().forEach(System.out::println);
    }
}
