package com.coffee.shop.run.service;

import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.OrderService;

public class CancelOrderRunner implements CoffeeShopBaseRunner {
    OrderService orderService = OrderService.getInstance();

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        orderService.cancelOrder(coffeeShopContext.getOrderNumber());
    }
}
