package com.coffee.shop.run.service;

import com.coffee.shop.entity.Customer;
import com.coffee.shop.entity.Order;
import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.CustomerService;
import com.coffee.shop.service.OrderService;
import org.apache.commons.lang.StringUtils;

import java.util.Scanner;

public class PlaceOrderRunner implements CoffeeShopBaseRunner {

    private OrderService orderService = OrderService.getInstance();

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        Customer customer = getCustomerDetails();
        if (null != customer) {
            orderService.placeOrder(coffeeShopContext.getOrderNumber(), customer);
        }else{
            System.out.println("Order not placed.");
        }
    }

    private Customer getCustomerDetails() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your name.");
        String name = scanner.nextLine();
        System.out.println("Please enter your email");
        String email = scanner.nextLine();
        if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(email)) {
            Customer customer = new Customer(name, email);
            return customer;
        } else {
            System.out.println("Customer name or email cannot be empty");
            return null;
        }
    }
}
