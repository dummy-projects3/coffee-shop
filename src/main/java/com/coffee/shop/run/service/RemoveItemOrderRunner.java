package com.coffee.shop.run.service;

import com.coffee.shop.entity.Drink;
import com.coffee.shop.entity.Order;
import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.service.DrinkService;
import com.coffee.shop.service.OrderService;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Scanner;

public class RemoveItemOrderRunner implements CoffeeShopBaseRunner {
    private OrderService orderService = OrderService.getInstance();
    private DrinkService drinkService = DrinkService.getInstance();

    @Override
    public void execute(CoffeeShopContext coffeeShopContext) {
        Order order = getOrderDetails(coffeeShopContext);
        if (null != order) {
            orderService.updateOrder(order);
            System.out.println("Drinks Removed From Order Susccessfully");
        }
    }

    private Order getOrderDetails(CoffeeShopContext coffeeShopContext) {
        Order dbOrder = orderService.getOrderByOrderId(coffeeShopContext.getOrderNumber());
        if (null == dbOrder) {
            System.out.println("Order with Order ID : " + coffeeShopContext.getOrderNumber() + " does not exists.");
            System.out.println("Please create order");
            return null;
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter drink name as per listed menu item to remove OR press ``SPACE BAR`` to go back main menu");
        String orderItem = scanner.nextLine();
        if (orderItem.equalsIgnoreCase(" ")) {
            return null;
        }
        Drink drink = getDrinkByName(orderItem);
        if (CollectionUtils.isEmpty(dbOrder.getDrinks())) {
            System.out.println("No drinks to remove.");
        } else {
            dbOrder.getDrinks().remove(drink);
        }
        return dbOrder;
    }


    private Drink getDrinkByName(String drinkName) {
        Drink drink = drinkService.getDrinkByName(drinkName);
        if (null == drink) {
            System.out.println("Entered drink doesn't exist. Please enter selected drink name.");
        }
        return drink;
    }
}
