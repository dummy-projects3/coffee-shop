package com.coffee.shop.run.service;

import com.coffee.shop.model.CoffeeShopContext;

public interface CoffeeShopBaseRunner {
    public void execute(CoffeeShopContext coffeeShopContext);
}
