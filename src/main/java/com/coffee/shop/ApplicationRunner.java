package com.coffee.shop;

import com.coffee.shop.model.CoffeeShopContext;
import com.coffee.shop.run.service.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ApplicationRunner {
    private static CoffeeShopContext coffeeShopContext = new CoffeeShopContext();
    private static Integer ORDER_NUMBER = 1;
    private static Map<Integer, CoffeeShopBaseRunner> menuItemListing = new HashMap<>();

    static {
        menuItemListing.put(1, new LoadMenuItemRunner());
        menuItemListing.put(2, new AddItemOrderRunner());
        menuItemListing.put(3, new RemoveItemOrderRunner());
        menuItemListing.put(4, new PlaceOrderRunner());
        menuItemListing.put(5, new CancelOrderRunner());
        menuItemListing.put(6, new CustomerOrderHistoryRunner());
    }

    public ApplicationRunner() {
        System.out.println("Welcome to Coffee Shop");
        loadMenu();
    }

    private void loadMenu() {
        boolean keepMenuAlive = true;
        while (keepMenuAlive) {
            coffeeShopContext.setOrderNumber(ORDER_NUMBER);
            System.out.println("1. Show the drinks menu\n" +
                    "2. Add drink to order\n" +
                    "3. Remove the drink from the order\n" +
                    "4. Place an order\n" +
                    "5. Cancel Order\n" +
                    "6. Get Customer Order History\n" +
                    "7. Exit\n");

            Scanner scanner = new Scanner(System.in);
            Integer menuItem = scanner.nextInt();
            if (menuItem.equals(7)) {
                keepMenuAlive = false;
                System.out.println("Exiting from the application.");
            } else if (menuItem > 0 && menuItem < 7) {
                CoffeeShopBaseRunner coffeeShopBaseRunner = menuItemListing.get(menuItem);
                coffeeShopBaseRunner.execute(coffeeShopContext);
                if (menuItem.equals(4) || menuItem.equals(5)) {
                    ORDER_NUMBER += 1;
                }
            } else {
                System.out.println("Please select correct option from menu!!!");
            }
        }
    }
}
